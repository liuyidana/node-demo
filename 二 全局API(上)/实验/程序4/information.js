const log=console.log
//cpu架构信息和操作系统
log('architecture:',process.arch);
log('cwd:%s\n',process.cwd());

//id信息
og('process id:',process.pid);
//nodejs可执行文件路径
log('exePath:%s\n',process.exePath);
log('cwd:%s\n',process.cwd());
//获取内存的使用情况
log('rss:',process.memoryUsage());