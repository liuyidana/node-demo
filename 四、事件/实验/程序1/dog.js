/*
var events=require("events");
var Emitter=events.EventEmitter;

function Dog(name,energy){
    this.name=name;
    this.energy=energy;
    Emitter.call(this);
}
Dog.prototype=Emitter.prototype;
Dog.prototype.Bark=function(){
    console.log(this.name+" "+"barked!" +"energy"+":"+this.energy)
}
module.exports=Dog;
*/

//老师写的
var events=require("events");
var EventEmitter=events.EventEmitter;

function Dog(name,energy){
    this.name=name;
    this.energy=energy;
    EventEmitter.call(this);
    var that =this;
    /*
        1、若使用function函数，则函数中this指向取决于调用主体是谁，this指向intervalId
        2、使用箭头函数，函数中为一个包裹体，从箭头函数的括号到1000之前，向上寻找this指向，发现为实例对象
        (函数内this指向函数定义的上下文)this指向dogBark.js中实例对象dog
    */
    var intervalId=setInterval(function(){
        console.log(this)
        if(this.energy>=0){
            this.emit("bark");
            this.energy--;
        }else{
            clearInterval(intervalId);
        }
    },1000)
}

for(var i in EventEmitter.prototype){
    Dog.prototype[i]=EventEmitter.prototype[i];
}
module.exports=Dog;
