var events=require("events");
var Emitter=events.EventEmitter;
var util=require("util");
function Radio(name,frequency){
    this.name=name;
    this.frequency=frequency;
    Emitter.call(this);
}
util.inherits(Radio,Emitter);
module.exports=Radio
