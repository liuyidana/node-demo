var Radio=require("./radio.js");
var radio=new Radio("music radio","FM 106.7");

//事件的监听
radio.on("play",function(){
    console.log('"'+radio.name+'"'+" "+radio.frequency+" "+"opened");
    
})
radio.on("stop",function(){
    console.log('"'+radio.name+'"'+" "+radio.frequency+" "+"closed");
})
//事件回调
radio.emit("play");

setTimeout(function(){
    console.log("lalala...");
},2000);
setTimeout(function(){
    radio.emit("stop");
},3000);
