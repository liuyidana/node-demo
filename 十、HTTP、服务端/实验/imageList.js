const http = require("http");
const fs = require("fs");
const path = require("path");
http.createServer((req,res)=>{

    if(req.url=='/'){
        var fileContent= fs.readFileSync("./index.html");
        res.writeHead(200,{"Content-Type":"text/html;charset=utf8"});
        res.end(fileContent);
    }else if(req.url=="/upload"){
        var str ='';
        req.setEncoding("binary");
        req.on("data",(chunk)=>{
            str+=chunk;
        })
        req.on("end",()=>{
            console.log(str);
            var arr = str.split("\r\n");
            console.log(arr);
            var imgArr = arr.slice(4,arr.length-2);
            console.log(imgArr);
            var imgStr = imgArr.join("\r\n");
            var buf = Buffer.from(imgStr,"binary");
            console.log(buf);
            fs.writeFileSync(path.join(__dirname,'/upload/2.jpg'),buf,("encoding","binary"));
        })
    }else if(req.url=='/getlist'){
        var files = fs.readdirSync(path.join(__dirname,"/upload"));
        res.writeHead(200,{'Content-Type':"text/plain"})
        res.end(JSON.stringify(files));
    }else if(req.url=='/list'){
        var fileContent = fs.readFileSync("./list.html");
        res.writeHead(200,{"Content-Type":"text/html;charset=utf8"});
        res.end(fileContent);
    }
}).listen(8081)