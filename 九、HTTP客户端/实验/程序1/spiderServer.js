/**
 * 收到请求时，把网页相应回去
 * http://localhost:8081/
 * 将index.html相应到前端
 */
const http=require("http");
const url=require("url");
const path=require("path");
const fs=require("fs")
const https=require("https");
const cheerio=require("cheerio")
//把8081端口号的服务器中的内容相应到node控制台
var reqUrl="https://maoyan.com/films";
var movieList=[];

http.createServer((req,res)=>{
    var urlObj=url.parse(req.url);
    var pathname=urlObj.pathname
    if(pathname=="/"){
        var filePath=path.join(__dirname,"index.html");
        fs.readFile(filePath,(err,data)=>{
            if(err){
                console.log(err)
            }
            else{
                res.writeHead(200,{"Content-Type":"text/html;charset=utf8"})
                res.write(data)
                res.end()
            }
        })
    }
    else if(pathname=="/getlist"){
        https.get(reqUrl,(resObj)=>{
            var htmlStr="";
            resObj.on("data",(chunk)=>{
                htmlStr+=chunk
            })
            resObj.on("end",()=>{
                var $=cheerio.load(htmlStr);
                console.log(htmlStr)
                $(".movie-item-title a").each((i,el)=>{
                    var movieName=$(el).text();
                    var movieId=$(el).attr("data-val");
                    movieId=movieId.slice(movieId.indexOf(":")+1,-1);
                    var num1=$(el).parent().next().children(".integer").text();
                    var num2=$(el).parent().next().children(".fraction").text();
                    var movieRate=num1+num2;
                    var movieObj={movieId:movieId,movieName:movieName,movieRate:movieRate}
                    movieList.push(movieObj)
                })
                console.log(movieList)
                res.writeHead(200,{"Content-Type":"text/plain"});
                res.write(JSON.stringify(movieList))
                res.end()
            })
        })
    }
}).listen(8081)