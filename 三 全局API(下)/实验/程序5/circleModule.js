function circleFun(r){
    this.r=r;
    var circumference;
    var area;
    circumference=function(r){
        return 2*3.14*r;
    }
    area=function(r){
        return 3.14*r*r;
    }

    
    return{
        circumference:circumference(r),
        area:area(r)

    }
}

module.exports=circleFun