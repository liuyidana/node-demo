var circleModule={
    circumference:function(r){
        return 3.14*2*r;
    },
    area:function(r){
        return r*r*3.14;
    }
}


module.exports=circleModule;