var http=require("http");
var fs=require("fs");
var path=require("path");

var dirPath=process.argv[2];
http.createServer((req,res)=>{
    if(dirPath!=""){
        fs.readdir(__dirname,(err,files)=>{
            if((files.indexOf(dirPath))!=-1){
                var filePath=path.join(__dirname, "/" + dirPath);//注意事项
                fs.readFile(filePath,(err,data)=>{
                    if(err){
                        console.log("无法读取数据")
                    }else{
                        res.write(data.toString("utf-8"));
                        res.end()
                    }
                }); 
            }
            else{
                console.log("该目录下没有此文件");
                res.end()
            }
        })
    }else{
        var filePath1=path.join(__dirname,"fileReader1.js")
        fs.readFile(filePath1,(err,data)=>{
            if(err){
                console.log(err)
            }else{
                res.write(data.toString("utf-8"));
                res.end()
            }
        });
    }
}).listen(8081) 
console.log("server is listening 8081")

