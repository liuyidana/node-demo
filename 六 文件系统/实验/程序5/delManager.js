var http=require("http");
var fs=require("fs");
var path=require("path");

var argv=process.argv[2]
var filePath=path.join(__dirname,"/"+argv)
if(fs.existsSync(filePath)){
    function del(){
        var files=fs.readdirSync(filePath);
        for(var i=0;i<files.length;i++){
            var childpath=path.join(filePath,"/"+files[i]);
            var fileObj=fs.statSync(childpath)
            var childName=files[i]
            if(fileObj.isFile()){
                fs.unlinkSync(childpath);
                console.log("删除成功")
            }
            else if(fileObj.isDirectory()){
                console.log("删除成功")
                del(childpath)
            }
        }
        fs.rmdirSync(filePath)
    }
    fs.stat(filePath,(err,stats)=>{
        if(err){
            console.log(err)
        }else if(stats.isFile()===true){
            fs.unlinkSync(filePath)
            console.log("删除成功")
        }else if(stats.isDirectory()==true){
            del(filePath)
        }
    })
}else{
    console.log("文件不存在")
}