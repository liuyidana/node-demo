var http=require("http");
var fs=require("fs");
var path=require("path");

var dirPath=process.argv[2];
//console.log(__dirname);
//console.log(__filename);
http.createServer((req,res)=>{
    if(dirPath!=null){
        fs.readdir(__dirname,(err,files)=>{
            if((files.indexOf(dirPath))!=-1){
                var filePath=path.join(__dirname, "/" + dirPath);//注意事项
                var readable=fs.createReadStream(filePath);
                readable.pipe(res)
            }else{
                console.log("该目录下没有此文件");
                res.end()
            }
        })
    
    }else{
        var filePath1=path.join(__dirname,"fileReader3.js");//注意事项
        var readable1=fs.createReadStream(filePath1);
        readable1.pipe(res);
    }
}).listen(8081)
console.log("server is listening 8081")