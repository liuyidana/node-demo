const http = require("http");
const fs = require("fs");
const path = require("path");
const argv = process.argv[2];

http.createServer((req,res)=>{
    if(argv){
res.writeHead(200,{"Content-Type":"text/html"});        
        fs.open(path.join(__dirname,"/"+argv),'r',function(err,fd){
            if(err){
                res.write(err)
                fs.close(fd);
            }else{
                var buffer = new Buffer(255);
                fs.read(fd, buffer, 0, 255, 0, function(err, bytesRead, buffer) {
                    if(err){
                        res.write(err)
                    }else{
                        res.write(buffer.toString())
                    }
                })

                fs.close(fd);
            }
        })
    }else{
        res.writeHead(200,{"Content-Type":"text/html"});
    fs.open(path.join(__dirname,"/fileReader2.js"),'r',function(err,fd){
        if(err){
            res.write(err)
            fs.close(fd);
        }else{
            var buffer = new Buffer(255);
            fs.read(fd, buffer, 0, 255, 0, function(err, bytesRead, buffer) {
                if(err){
                    res.write(err)
                }else{
                    res.write(buffer.toString())
                }
            })

            fs.close(fd);
        }
    })
}
}).listen(8081);

console.log("sever is listening 8081");