var fs=require("fs");
var path=require("path");

//获取某个文件中的内容
var firstfilePath=path.join(__dirname,"/from.txt")
var readable=fs.createReadStream(firstfilePath)
//创建可写流
var lastfilePath=path.join(__dirname,"/to.txt");
var writable=fs.createWriteStream(lastfilePath);


readable.on("data",function(chunk){
    readable=chunk.toString("utf8").toLocaleUpperCase();
})
readable.pipe(writable)

